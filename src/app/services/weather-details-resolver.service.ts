import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Weather } from '../models/weather.model';
import { WeatherService } from './weathers.service';

@Injectable({
  providedIn: 'root',
})
export class WeatherDetailsResolverService implements Resolve<Weather> {
  constructor(
    private weathersService: WeatherService,
    private router: Router
  ) {}

  resolve(route: ActivatedRouteSnapshot) {
    if (!this.weathersService.getWeathers().length)
      this.weathersService.loadWeathers();
    const { city } = route.params;
    const weather = this.weathersService
      .getWeathers()
      .find((weather) => weather.city === city);
    if (!weather) this.router.navigate(['not-found']);
    return weather;
  }
}
