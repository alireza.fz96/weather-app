import { Weather } from '../models/weather.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Injectable()
export class WeatherService {
  weathers: Weather[];
  weathersChanged = new Subject<Weather[]>();

  constructor(private http: HttpClient) {
    this.weathers = []
  }

  getWeatherByCity(city: string) {
    return this.weathers.find((weather) => weather.city === city);
  }

  getWeathers() {
    return this.weathers.slice();
  }

  fetchWeather(cityName: string) {
    return this.http
      .get<{ list: any }>('https://api.openweathermap.org/data/2.5/forecast', {
        params: {
          q: cityName,
          appid: 'c28434fcae90783ce5c47578893ca192',
        },
      })
      .pipe(map((res) => this.createWeather(res.list, cityName)));
  }

  createWeather(weathers: any[], city: string): Weather {
    let newWeather = this.extractWeather(weathers[0], city);
    let futureDaysWeather: Weather[] = [];
    for (let i = 7; i < 40; i += 8)
      futureDaysWeather.push(this.extractWeather(weathers[i], city));
    newWeather.futureDaysWeather = futureDaysWeather;
    return newWeather;
  }

  extractWeather(data: any, city: string): Weather {
    const { main, weather, wind, dt } = data;
    return {
      main,
      weather,
      wind,
      city,
      date: new Date(dt * 1000),
    };
  }

  addWeather(weather: Weather) {
    this.weathers.push(weather);
    this.storeWeathers();
    this.weathersChanged.next(this.weathers.slice());
  }

  storeWeathers() {
    localStorage.setItem('weathers', JSON.stringify(this.weathers));
  }

  loadWeathers() {
    const weathers = JSON.parse(localStorage.getItem('weathers'));
    this.weathers = weathers || [];
    this.weathersChanged.next(this.weathers);
  }
}
