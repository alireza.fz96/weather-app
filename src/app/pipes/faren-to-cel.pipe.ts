import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'kelvinToSelsius',
})
export class KelvinToSelsius implements PipeTransform {
  transform(value: number): string {
    return (value - 273).toFixed();
  }
}
