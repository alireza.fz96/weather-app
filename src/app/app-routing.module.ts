import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { WeatherDetailsComponent } from './components/weather-details/weather-details.component';
import { AddCityComponent } from './components/add-city/add-city.component';
import { WeatherDetailsResolverService } from './services/weather-details-resolver.service';
import { ErrorPageComponent } from './components/error-page/error-page.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'weathers/:city',
    component: WeatherDetailsComponent,
    resolve: [WeatherDetailsResolverService],
  },
  { path: 'add-city', component: AddCityComponent },
  {
    path: 'not-found',
    component: ErrorPageComponent,
    data: { error: '404 not found' },
  },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
