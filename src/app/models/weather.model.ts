export interface Weather {
  city: string;
  date: Date;
  main: { temp: number; temp_max: number; temp_min: number; humidity: number };
  weather: { main: string }[];
  wind: { speed: number };
  futureDaysWeather?: Weather[];
}
