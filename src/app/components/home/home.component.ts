import { Component, OnInit, OnDestroy } from '@angular/core';
import { WeatherService } from '../../services/weathers.service';
import { Weather } from '../../models/weather.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  weathers: Weather[];
  weathersSub: Subscription;

  constructor(private weatherService: WeatherService) {}

  ngOnInit() {
    this.weathersSub = this.weatherService.weathersChanged.subscribe(
      (weathers) => (this.weathers = weathers)
    );
    this.weatherService.loadWeathers();
  }

  ngOnDestroy() {
    this.weathersSub.unsubscribe();
  }
}
