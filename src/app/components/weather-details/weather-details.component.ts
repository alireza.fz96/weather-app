import { Component, OnInit } from '@angular/core';
import { Weather } from '../../models/weather.model';
import { ActivatedRoute } from '@angular/router';
import { WeatherService } from '../../services/weathers.service';

@Component({
  selector: 'app-weather-details',
  templateUrl: './weather-details.component.html',
  styleUrls: ['./weather-details.component.scss'],
})
export class WeatherDetailsComponent implements OnInit {
  weather: Weather;

  constructor(
    private route: ActivatedRoute,
    private weatherService: WeatherService
  ) {}

  ngOnInit() {
    const city = this.route.snapshot.params.city;
    this.weather = this.weatherService.getWeatherByCity(city);
  }
}
