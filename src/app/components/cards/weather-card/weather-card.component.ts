import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Weather } from '../../../models/weather.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.scss'],
})
export class WeatherCardComponent implements OnInit {
  @Input() weather: Weather;
  @Input() addMode = false;
  @Output() addWeather = new EventEmitter<Weather>();

  constructor(private router: Router) {}

  ngOnInit(): void {}

  onAddWeather() {
    this.addWeather.emit(this.weather);
  }

  onCardClicked() {
    if (!this.addMode) this.router.navigate(['/weathers', this.weather.city]);
  }
}
