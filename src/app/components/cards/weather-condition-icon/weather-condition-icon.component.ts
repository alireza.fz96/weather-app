import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-weather-condition-icon',
  templateUrl: './weather-condition-icon.component.html',
  styleUrls: ['./weather-condition-icon.component.scss'],
})
export class WeatherConditionIconComponent implements OnInit {
  @Input() condition: string;
  constructor() {}

  ngOnInit(): void {}
}
