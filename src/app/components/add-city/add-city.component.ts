import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Weather } from '../../models/weather.model';
import { WeatherService } from '../../services/weathers.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-city',
  templateUrl: './add-city.component.html',
  styleUrls: ['./add-city.component.scss'],
})
export class AddCityComponent implements OnInit {
  @ViewChild('cityName') cityNameInput: ElementRef;

  weather: Weather;
  error: '';
  isLoading = false;

  constructor(
    private weathersService: WeatherService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  onSearchCity() {
    let cityName = this.cityNameInput.nativeElement.value;
    this.isLoading = true;
    this.weathersService.fetchWeather(cityName).subscribe(
      (weather) => {
        this.weather = weather;
        this.error = '';
        this.isLoading = false;
      },
      (error) => {
        this.weather = null;
        this.error = error.error.message;
        this.isLoading = false;
      }
    );
  }

  onEnter(event: KeyboardEvent) {
    if (event.keyCode === 13) this.onSearchCity();
  }

  onAddCity() {
    this.weathersService.addWeather(this.weather);
    this.router.navigate(['']);
  }
}
