import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { WeatherCardComponent } from './components/cards/weather-card/weather-card.component';
import { WeatherService } from './services/weathers.service';
import { NavbarComponent } from './components/navbar/navbar.component';
import { WeatherDetailsComponent } from './components/weather-details/weather-details.component';
import { WeatherConditionIconComponent } from './components/cards/weather-condition-icon/weather-condition-icon.component';
import { AddCardComponent } from './components/cards/add-card/add-card.component';
import { WeatherInfoComponent } from './components/weather-details/weather-info/weather-info.component';
import { AddCityComponent } from './components/add-city/add-city.component';
import { KelvinToSelsius } from './pipes/faren-to-cel.pipe';
import { ErrorPageComponent } from './components/error-page/error-page.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    WeatherCardComponent,
    NavbarComponent,
    WeatherDetailsComponent,
    WeatherConditionIconComponent,
    AddCardComponent,
    WeatherInfoComponent,
    AddCityComponent,
    KelvinToSelsius,
    ErrorPageComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [WeatherService],
  bootstrap: [AppComponent],
})
export class AppModule {}
